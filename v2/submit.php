<?php
/*Submit the  form values */
$name1_err=$name2_err=$name3_err=$number1_err=$number2_err=$number3_err="";
$name_err_msg_text=$number_err_msg_text="";
$return_msg  ="";

if(isset($_POST['submit'])){
	
	$formFields = array('name1', 'name2', 'name3', 'healthid1', 'healthid2', 'healthid3');
	$formData = getFormData($formFields);
	
	if(!empty($formData['namenotValidFields'])){
		$name_err = $formData['namenotValidFields'];
		$name_err_msg_text = "should be at least 3 Character";
		
		if(!empty($name_err[0])){
			$name1_err = $name_err[0].' '.$name_err_msg_text;
		}
		if(!empty($name_err[1])){
			$name2_err = $name_err[1].' '.$name_err_msg_text;
		}
		if(!empty($name_err[2])){
			$name3_err = $name_err[2].' '.$name_err_msg_text;
		}
		$return_msg = "ERROR";
		
	}
	if(!empty($formData['numbernotValidFields'])){
		$number_err = $formData['numbernotValidFields'];
		$number_err_msg_text = "should be at equal to 5 digit Number";
		
		if(!empty($number_err[0])){
			$number1_err = $number_err[0].' '.$number_err_msg_text;
		}
		if(!empty($number_err[1])){
			$number2_err = $number_err[1].' '.$number_err_msg_text;
		}
		if(!empty($number_err[2])){
			$number3_err = $number_err[2].' '.$number_err_msg_text;
		}
		$return_msg = "ERROR";
	}
	
	if( (empty($formData['namenotValidFields'])) && (empty($formData['numbernotValidFields'])) ){
		$return_msg = "SUCCESS";
	}
	
}

function name_validation($name){
	$length = strlen($name);
	if($length>=3){ 
		$success ="";
		return $success;
	}else{ 
		$error = "Name should be at least 3 Character"; 
			return $error;
	}
}

function number_validation($number){ 
	$length = strlen($number);
	if( (preg_match("/^[0-9]+$/i", $number) ) && ($length == 5)  ){
			$success ="";
			return $success;
	}else{
		$error = "HealthID should be at least 5 Number"; 
			return $error;
	}
}


function getFormData($formFields){
       $formData = array();
       $formData['valid'] = true;
       $formData['fields'] = array();
       $formData['notValidFields'] = array();
       $formData['namenotValidFields'] = array();
       $formData['numbernotValidFields'] = array();
	   
	   $nameArr = array('name1', 'name2', 'name3');
	   $numberArr = array('healthid1', 'healthid2', 'healthid3');

       for($a = 0; $a < count($formFields); $a++){
               $field = $formFields[$a];
               if(isset($_POST[$field])){
                       $value = $_POST[$field];
                       if(empty($value)){
                               $formData['valid'] = false;
                               $formData['notValidFields'][] = $field;
                       }else{
								if(in_array($field, $nameArr)){ 
								   if(name_validation($value) !=""){ 
										$formData['namenotValidFields'][] = $field;
								   }
								}elseif(in_array($field, $numberArr)){
									if(number_validation($value) !=""){
										$formData['numbernotValidFields'][] = $field;
									}
							    }else{
									$formData['fields'][$field] = $value;
								}							   
							   
                       }
               }else{
                       $formData['valid'] = false;
                       $formData['notValidFields'][] = $field;
               }
       }
       return $formData;
}

?>
<style>
.error{
	color: red;
	font-weight: bold;
}
.success{
	color: green;
	font-weight: bold;
}
</style>
<span <?php if($return_msg == "SUCCESS"){ ?> class="success" <?php }else{ ?> class="error" <?php } ?>><?php echo $return_msg; ?> </span>
<ul>
<?php if($name1_err !=""){ echo '<li><span class="error">'.$name1_err.'</span></li>'; }
if($number1_err !=""){ echo '<li><span class="error">'.$number1_err.'</span></li>'; }
if($name2_err !=""){ echo '<li><span class="error">'.$name2_err.'</span></li>'; }
if($number2_err !=""){ echo '<li><span class="error">'.$number2_err.'</span></li>'; }
if($name3_err !=""){ echo '<li><span class="error">'.$name3_err.'</span></li>'; }
if($number3_err !=""){ echo '<li><span class="error">'.$number3_err.'</span></li>'; }
?>
</ul>
<form action=" " method="post">
  <label for="name1">Name 1</label><br>
  <input type="text" id="name1" name="name1" value="<?php echo @$_POST['name1'];?>"><br>
  <label for="healthid1">Health 1</label><br>
  <input type="text" id="healthid1" name="healthid1" value="<?php echo @$_POST['healthid1'];?>"><br><br>
  <label for="name2">Name 2</label><br>
  <input type="text" id="name2" name="name2" value="<?php echo @$_POST['name2'];?>"><br><br>
  <label for="healthid2">Health 2</label><br>
  <input type="text" id="healthid2" name="healthid2" value="<?php echo @$_POST['healthid2'];?>"><br><br>
  <label for="name3">Name 3</label><br>
  <input type="text" id="name3" name="name3" value="<?php echo @$_POST['name3'];?>"><br>
  <label for="healthid3">Health 3</label><br>
  <input type="text" id="healthid3" name="healthid3" value="<?php echo @$_POST['healthid3'];?>"><br><br>
  <input type="submit" name="submit" value="Submit">
</form> 