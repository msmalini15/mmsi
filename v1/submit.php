<?php
/*Submit the  form values */
$name1_err=$health1_err="";
if(isset($_POST['submit'])){
	
	$empty_msg = "Required field";
	
	if(empty($_POST['name1'])){
		$name1_err = $empty_msg;
	}else if(!empty($_POST['name1'])){
		if(name_validation($_POST['name1'])!=""){
			$name1_err = name_validation($_POST['name1']);
		}
	} //Name1 Validation
	
	if(empty($_POST['healthid1'])){
		$health1_err = $empty_msg;
	}else if(!empty($_POST['healthid1'])){
		if(number_validation($_POST['healthid1'])!= ""){
			$health1_err = number_validation($_POST['healthid1']);
		}
	} // HealthID1 Validation
	
	if( ($name1_err=="") && ($health1_err=="")){
		$return_msg = "SUCCESS"; //exit;
	}else{
		$return_msg = "ERROR"; //exit;
	}
	
}

function name_validation($name){
	$length = strlen($name);
	if($length>=3){ 
		$success ="";
		return $success;
	}else{ 
		$error = "Name should be at least 3 Character"; 
			return $error;
	}
}

function number_validation($number){ 
	$length = strlen($number);
	if( (preg_match("/^[0-9]+$/i", $number) ) && ($length == 5)  ){
			$success ="";
			return $success;
	}else{
		$error = "HealthID should be at least 5 Number"; 
			return $error;
	}
}
?>
<style>
.error{
	color: red;
	font-weight: bold;
}
.success{
	color: green;
	font-weight: bold;
}
</style>
<span <?php if($return_msg == "SUCCESS"){ ?> class="success" <?php }else{ ?> class="error" <?php } ?>><?php echo $return_msg; ?> </span>
<form action=" " method="post">
  <label for="name1">Name 1</label><br>
  <input type="text" id="name1" name="name1" value="<?php echo $_POST['name1'];?>"><span class="error"><?php echo $name1_err;?></span><br>
  <label for="healthid1">Health 1</label><br>
  <input type="text" id="healthid1" name="healthid1" value="<?php echo $_POST['healthid1'];?>"><span class="error"><?php echo $health1_err;?></span><br><br>
  <input type="submit" name="submit" value="Submit">
</form> 