<?php
/**
 * MMSI Theme functions and definitions
 *
 * @package WordPress
 * @subpackage mmsi Theme
 * @since mmsi Theme 1.0
 */

function automobiles_setup_post_type() {
    $args = array(
        'public'    => true,
        'label'     => __( 'Automobiles', 'textdomain' ),
        'menu_icon' => 'dashicons-book',
    );
    register_post_type( 'automobiles', $args );
}
add_action( 'init', 'automobiles_setup_post_type' );