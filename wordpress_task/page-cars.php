<?php
/**
 * Template Name: Cars Template
 * 
 */

//get_header();
?>


	<?php
	    $args = array(  
        'post_type' => 'automobiles',
        'post_status' => 'publish',
        'posts_per_page' => -1, 
        'orderby' => 'title', 
        'order' => 'ASC', 
    );

    $loop = new WP_Query( $args ); 
    if ( $loop->have_posts() ) {
		echo '<ul>';
			while ( $loop->have_posts() ) : $loop->the_post(); 
				echo '<li>'.get_the_title().'</li>'; 
			endwhile;
		echo '</ul>';	
			 wp_reset_postdata(); 
	}else{
		echo "No Posts found";
	}

   
	?>


<?php //get_footer(); ?>
